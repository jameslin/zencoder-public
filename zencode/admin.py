from django.contrib import admin
from django.db import models
from zencode.models import Thumb, ZencodedVideo

#class ThumbInline(admin.StackedInline):
class ThumbInline(admin.TabularInline):
    extra = 0
    max_num = 0
    model = Thumb

class ZencodedVideoAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    inlines = (ThumbInline,)
    list_display = ('pk', 'created', 'label', 'is_completed', 'has_result', 'has_notification', 'has_details', 'video',) # 'has_video',
    list_display_links = ('pk', 'created',)
    ordering = ('-created',)
#     prepopulated_fields = {'slug': ('name',)}
#     raw_id_fields = ('owner',)
#    readonly_fields = ('zencoder_id', 'zencoder_result', 'zencoder_notification', 'zencoder_details',)
    save_on_top = True
#     search_fields = ('name', 'description',)

    def has_result(self, video):
        return bool(video.zencoder_result)
    has_result.boolean = True
    has_result.admin_order_field = 'zencoder_result'

    def has_notification(self, video):
        return bool(video.zencoder_notification)
    has_notification.boolean = True
    has_notification.admin_order_field = 'zencoder_notification'

    def has_details(self, video):
        return bool(video.zencoder_details)
    has_details.boolean = True
    has_details.admin_order_field = 'zencoder_details'

    def is_completed(self, video):
        return bool(video.completed)
    is_completed.boolean = True
    is_completed.admin_order_field = 'completed'
admin.site.register(ZencodedVideo, ZencodedVideoAdmin)
