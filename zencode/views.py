# coding=utf-8
from datetime import date, datetime
import json, urllib2, urlparse
from os import makedirs
from os.path import basename, exists, join, splitext
from uuid import uuid1
from django.conf import settings
from django.core.mail import mail_managers
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import Context, RequestContext, Template
from django.views.decorators.csrf import csrf_exempt
from zencode.models import ZencodedVideo, Thumb
from zencode.tasks import get_zencoded_video

@csrf_exempt
def zencoded(request):
    # If not what we expect, we'll throw a 500 ... which is good
    response = json.loads(request.raw_post_data)
    video_id = response['output']['id']

    video = get_object_or_404(ZencodedVideo, zencoder_id=video_id, completed__isnull=True) # TODO Someone who knows the zencoder id *and* sends a bogus notification before Zencoder could trick us into accepting their video    
    video.zencoder_notification = request.raw_post_data

    video_state = response['output']['state']
    if video_state == 'finished': # yay
        job_id = response['job']['id'] # Needed to get details (thumbs, dimensions, etc)
        video_source = response['output']['url']
        video_ext = splitext(video_source)[1].split('?')[0] # splitext includes S3's querystring auth tokens, hence the split()
        filename = str(uuid1())+video_ext

        # Fetch the vid with celery
        upload_to = date.today().strftime(video.video.field.upload_to)
        if not exists(join(settings.MEDIA_ROOT, upload_to)):
            makedirs(join(settings.MEDIA_ROOT, upload_to))
        video_dest = join(upload_to, filename)
        video.save()
        get_zencoded_video.delay(job_id, video, video_source, video_dest)

    return HttpResponse("Got it: %s %s. Thanks muchly" % (video_id, video_state))
