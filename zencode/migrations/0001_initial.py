# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ZencodedVideo'
        db.create_table('zencode_zencodedvideo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('input', self.gf('django.db.models.fields.URLField')(max_length=500)),
            ('label', self.gf('django.db.models.fields.CharField')(default='h264', max_length=6)),
            ('video', self.gf('django.db.models.fields.files.FileField')(db_index=True, max_length=200, blank=True)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('zencoder_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True, null=True, blank=True)),
            ('zencoder_result', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('zencoder_notification', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('zencoder_details', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('completed', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal('zencode', ['ZencodedVideo'])

        # Adding unique constraint on 'ZencodedVideo', fields ['content_type', 'object_id', 'label']
        db.create_unique('zencode_zencodedvideo', ['content_type_id', 'object_id', 'label'])

        # Adding model 'Thumb'
        db.create_table('zencode_thumb', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('video', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['zencode.ZencodedVideo'])),
            ('thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=200)),
            ('preferred', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal('zencode', ['Thumb'])


    def backwards(self, orm):
        # Removing unique constraint on 'ZencodedVideo', fields ['content_type', 'object_id', 'label']
        db.delete_unique('zencode_zencodedvideo', ['content_type_id', 'object_id', 'label'])

        # Deleting model 'ZencodedVideo'
        db.delete_table('zencode_zencodedvideo')

        # Deleting model 'Thumb'
        db.delete_table('zencode_thumb')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'zencode.thumb': {
            'Meta': {'ordering': "('-preferred',)", 'object_name': 'Thumb'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preferred': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '200'}),
            'video': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['zencode.ZencodedVideo']"})
        },
        'zencode.zencodedvideo': {
            'Meta': {'ordering': "('created',)", 'unique_together': "(('content_type', 'object_id', 'label'),)", 'object_name': 'ZencodedVideo'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'completed': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input': ('django.db.models.fields.URLField', [], {'max_length': '500'}),
            'label': ('django.db.models.fields.CharField', [], {'default': "'h264'", 'max_length': '6'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'video': ('django.db.models.fields.files.FileField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'zencoder_details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'zencoder_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'zencoder_notification': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'zencoder_result': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['zencode']