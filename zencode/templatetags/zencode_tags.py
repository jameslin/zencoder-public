from __future__ import division # http://docs.python.org/release/2.2.3/whatsnew/node7.html
from mimetypes import guess_type
from django import template
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.template.defaultfilters import stringfilter
#from django.utils.safestring import mark_safe
from zencode.models import ZencodedVideo



register = template.Library()

CODECS = {
    'video/mp4': 'avc1.42E01E, mp4a.40.2',
    'video/webm': 'vp8, vorbis',
    'video/ogg': 'theora, vorbis',
}


def zencode_has_video_with_label(obj, label):
    ct = ContentType.objects.get_for_model(obj)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk, label=label)
    return bool(vids)
    
def zencode_get_video_with_label(obj, label):
    ct = ContentType.objects.get_for_model(obj)
    return ZencodedVideo.objects.get(content_type=ct.pk, object_id=obj.pk, label=label)
    
def _zencode_video_thumb(obj):
    ct = ContentType.objects.get_for_model(obj)
    # TODO deliver mobile and desktop h.264 versions
    # Mobiles need baseline
    #vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk).exclude(label='mobile')
    for vid in vids:
        try:
            return vid.first_thumb()
        except:
            pass
    return u''

@register.simple_tag
def video_tag(obj, width=None, preload="none",):
    ct = ContentType.objects.get_for_model(obj)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk).exclude(video='')
    if not vids.count():
        return u''

    # calculate correct width, height
    if width:
        height = int(vids[0].height * (width / vids[0].width))
    else:
        height = vids[0].height
        width = vids[0].width
        
#    result = u'<video class="jwplayer" height="%d" width="%d" preload="%s" poster="%s" controls src="%s">' % (height, width, preload, _zencode_video_thumb(obj).url, vids[0].video.url)
#    result = u'<video class="jwplayer" src="%s" poster="%s">' % (vids[0].video.url, _zencode_video_thumb(obj).url)
    result = u'<video class="jwplayer" height="%d" width="%d" preload="%s" poster="%s" controls id="bogus">' % (height, width, preload, _zencode_video_thumb(obj).url)
    for vid in vids:
        mime = guess_type(vid.video.url)[0]
        result += u'<source src="%s" type=\'%s; codecs="%s"\' />' % (vid.video.url, mime, CODECS.get(mime, ''))
    result += u'</video>'
    return result
