from urlparse import urljoin
from zencoder import Zencoder
from django.conf import settings
from django.contrib.sites.models import Site

site = Site.objects.get_current()
watermark_url = "http://%s%swatermark_%s.png" % (site.domain, settings.STATIC_URL, settings.SITE_ID)

# the videos that we want
h264 = {
    "label": "h264",
    "video_codec": "h264",
#     "url": "s3://output-bucket/output-file-2-name.mp4",
#    "filename": "%s.mp4" % output_filename,
    "speed": settings.ZENCODER_SPEED,
    "width": settings.ZENCODER_DEFAULT_WIDTH,
    "watermark": {
        "url": watermark_url,
        "x": "-10",
        "y": "-10%",
        "width": "10%",
#        "height" : "80",
    },
    "thumbnails": {
        "number": 6,
#        "prefix": "output-thumb",
        "format": "jpg",    
    },
    "notifications": [
        "http://%s/zencoded" % site.domain,
        settings.ADMINS[0][1],
    ],
    "region": "us",
}

vp8 = {
    "label": "vp8",
    "video_codec": "vp8",
#    "url": "s3://output-bucket/output-file-2-name.webm",
#    "filename": "%s.webm" % output_filename,
    "speed": settings.ZENCODER_SPEED,
    "width": settings.ZENCODER_DEFAULT_WIDTH,
    "watermark": {
        "url": watermark_url,
        "x": "-10",
        "y": "-10%",
    },
    "notifications": [
        "http://%s/zencoded" % site.domain,
        settings.ADMINS[0][1],
    ],
    "region": "us",
}
 
mobile = {
    "label": "mobile",
    "video_codec": "h264",
#    "url": "s3://output-bucket/output-file-1-name.mp4",
#    "filename": "%s-iphone.mp4" % output_filename,
    "speed": settings.ZENCODER_SPEED,
    "width": "480",
    "height": "320",
    "watermark": {
        "url": watermark_url,
        "x": "-10",
        "y": "-10%",
    },
    "notifications": [
        "http://%s/zencoded" % site.domain,
        settings.ADMINS[0][1],
    ],
    "region": "us",
}

#outputs = (h264, vp8, mobile,)
outputs = (h264,)

def zencode(sender, instance, created, **kwargs):
    from zencode.models import ZencodedVideo
    if not instance.input:
        return False
    

    input = urljoin('http://'+site.domain+settings.MEDIA_URL, instance.input.url)

    #Checking whether a zencode job has already been run, return if it has
    #Works becuase every input field in the ZencodedVideo model is unique for each uploaded video due to a and append
    z = ZencodedVideo.objects.filter(input=input)

    if z.count():
        return



    zen = Zencoder(settings.ZENCODER_API_KEY)
    job = zen.job.create(input=input, outputs=outputs)

    
    for vid in job.body['outputs']:
        videos = instance.videos.filter(label=vid['label'])
        

        if videos.count():
            video = videos[0]
            video.input = input
            video.zencoder_result = vid
            video.zencoder_id = vid['id']
            video.zencoder_notification = ''

            video.video = ''
            video.completed = None
            video.active = False
            video.zencoder_details = ''
            video.height = None
            video.width = None
            video.duration = None
            # instance must define a property called owner which returns the User instance
            video.owner = instance.owner

            video.save()

        else:
            ZencodedVideo(content_object=instance, input=input, label=vid['label'], zencoder_result=vid, zencoder_id=vid['id'], owner=instance.owner).save()
