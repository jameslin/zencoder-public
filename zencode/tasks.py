from celery.task import task
from datetime import datetime
from os.path import basename, join
import urllib2
from zencode.models import Thumb
from zencoder import Zencoder
from django.conf import settings
from django.core.mail import mail_managers
from sslfix import HTTPSHandlerV3
from django.core.mail import send_mail
from django.contrib.sites.models import Site

@task()
def get_zencoded_video(job_id, video, video_source, video_dest):
    # get some details about this job
    zen = Zencoder(settings.ZENCODER_API_KEY)
    job_details = zen.job.details(job_id)
    job_output = job_details.body['job']['output_media_files'][0]
    height = job_output['height']
    width = job_output['width']
    duration = int(job_output['duration_in_ms'] / 1024)
    
    # get the thumbnails first
    thumbs = job_details.body['job']['thumbnails']
    thumb_urls = []
    for thumb in thumbs:
        # TODO thumb_ext = splitext(thumb['url'].split('?')[0])[1]
        thumb_ext = '.jpg'
        thumb_dest = "%s.%s%s" % (video_dest, thumb['id'], thumb_ext)
        local_dest = join(settings.MEDIA_ROOT, thumb_dest)
        
        
        if thumb['url'].startswith('https'):
            urllib2.install_opener(urllib2.build_opener(HTTPSHandlerV3()))
        remote_conn = urllib2.urlopen(thumb['url'])
            
        f = open(local_dest, 'wb')
        thumb_file = remote_conn.read()
        f.write(thumb_file)
        f.close()

        thumb_urls.append(thumb_dest)

        Thumb(video=video, thumb=thumb_dest).save()

    # then the video
    local_dest = join(settings.MEDIA_ROOT, video_dest)
    remote_conn = urllib2.urlopen(video_source)
        
    f = open(local_dest, 'wb')
    video_file = remote_conn.read()
    f.write(video_file)
    f.close()
    subject = 'Just fetched a zencoded file'
    body = "In:\n%s\n\nOut:\n%s\n\nThumbs:\n%s" % (video_source, local_dest, "\n".join(thumb_urls))

    mail_managers(subject, body)

    #Mail Users ...
    site = Site.objects.get_current()
    
    to_email = video.owner.email
    
    video_url = "http://" + site.domain
    
    success_message = "Hello,\n\n"
    success_message += "Thanks for uploading your video to {0}. Your video is now online at {1}\n\n".format(site.name, video_url)
    success_message += "The {0} team".format(site.name)
    
    success_subject = 'Your video is now online at {0}'.format(site.name)
    
    send_mail(success_subject, success_message, settings.DEFAULT_FROM_EMAIL, [to_email, ])

    video.video = video_dest
    video.completed = datetime.now()
    video.active = True
    video.zencoder_details = job_details.body
    if height:
        video.height = height
    if width:
        video.width = width
#     if duration:
#         video.duration = duration
    video.save()

    return True

