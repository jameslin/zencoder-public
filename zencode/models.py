from mimetypes import guess_type
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.sites.models import Site

site = Site.objects.get_current()

class ZencodedVideo(models.Model):
    LABEL_CHOICES = (
        ('h264', 'h.264',),
        ('vp8', 'VP8',),
        ('mobile', 'Mobile',),
    )
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    input = models.URLField(max_length=500)
    label = models.CharField(max_length=6, choices=LABEL_CHOICES, default='h264')
    video = models.FileField(db_index=True, upload_to='videos/zencoded/%Y/%m/', blank=True, max_length=200)
    height = models.PositiveSmallIntegerField(blank=True, null=True)
    width = models.PositiveSmallIntegerField(blank=True, null=True)
    zencoder_id = models.PositiveIntegerField(db_index=True, blank=True, null=True)
    zencoder_result = models.TextField(blank=True)
    zencoder_notification = models.TextField(blank=True)
    zencoder_details = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    completed = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(db_index=True, default=False)
    owner = models.ForeignKey(User, blank=True, null=True)
    
    def __unicode__(self):
        return u"%s (%s)" % (self.content_type, self.object_id)

    def first_thumb(self):
        return Thumb.objects.filter(video=self)[0].thumb
        
    class Meta:
        ordering = ('created',)
        unique_together = (
            ('content_type', 'object_id', 'label',),
        )
    
class Thumb(models.Model):
    video = models.ForeignKey(ZencodedVideo)
    thumb = models.ImageField(upload_to='videos/zencoded/%Y/%m/', max_length=200)
    preferred = models.BooleanField(db_index=True, default=False)

    def __unicode__(self):
        return self.thumb.url
    
    class Meta:
        ordering = ('-preferred',)

# utility functions

def _codecs_for_type(mimetype):
    codecs = {
        'video/mp4': 'avc1.42E01E, mp4a.40.2',
        'video/webm': 'vp8, vorbis',
        'video/ogg': 'theora, vorbis',
    }
    return codecs.get(mimetype, "")
    
def zencode_has_video_with_label(obj, label):
    ct = ContentType.objects.get_for_model(obj)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk, label=label)
    return bool(vids)
    
def zencode_get_video_with_label(obj, label):
    ct = ContentType.objects.get_for_model(obj)
    return ZencodedVideo.objects.get(content_type=ct.pk, object_id=obj.pk, label=label)
    
def zencode_video_tag(obj, preload="none"):
    ct = ContentType.objects.get_for_model(obj)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk).exclude(video='')
    if not vids.count():
        return u''
    result = u'<video class="jwplayer" height="%d" width="%d" preload="%s" poster="%s" controls>' % (vids[0].height, vids[0].width, preload, zencode_video_thumb(obj).url)
    for vid in vids:
        mime = guess_type(vid.video.url)[0]
        result += u'<source src="%s" type=\'%s; codecs="%s"\'>' % (vid.video.url, mime, _codecs_for_type(mime))
    result += u'</video>'
    return result

def zencode_video_thumb(obj):
    ct = ContentType.objects.get_for_model(obj)
    # TODO deliver mobile and desktop h.264 versions
    # Mobiles need baseline
    #vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk)
    vids = ZencodedVideo.objects.filter(content_type=ct.pk, object_id=obj.pk).exclude(label='mobile')
    for vid in vids:
        try:
            return vid.first_thumb()
        except:
            pass
    return u''
